---
layout: handbook-page-toc
title: "Entity-Specific Employment Policies"
description: "A directory of employment-related policies categorized by entity"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

This page will serve as a directory for our team members employed through a GitLab entity to locate employment-related policies specific to their country of residence. These policies do not supersede [GitLab Inc.'s Code of Business Conduct & Ethics](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/). 

## GitLab Entities

<details>
<summary>GitLab BV (Belgium)</summary>

### Belgium

</details>

<details>
<summary>GitLab BV (Netherlands)</summary>

### Netherlands 

**Health and Safety**
* [Safety Regulations for Remote Work](https://docs.google.com/document/d/1tKqPcjPeEgWdYWol3X8YWhqcbbS6SWrJ/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true)

</details>

<details>
<summary>GitLab Canada Corp.</summary>

</details>

<details>
<summary>GitLab France S.A.S.</summary>

### France 

</details>

<details>
<summary>GitLab GmbH (Germany)</summary>

### Germany 

**Health and Safety**

To ensure the health and safety of our team members in Germany, and to maintain compliance with the German Occupational Safety and Health Act, all team members in Germany will complete the following checklist at onboarding. This checklist will be reviewed annually. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, or you need to report an incident, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com).

* [Work from Home Checklist](https://docs.google.com/document/d/1Z3i-vrkcU5ald0j-scf2rgTumwFCc_sI/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true)
</details>

<details>
<summary>GitLab GK (Japan)</summary>

### Japan

</details>

<details>
<summary>GitLab Inc. (US)</summary>

### United States

**Health and Safety**
* [Workplace Evaluation](https://docs.google.com/document/d/12o3YXX6fIi2-V7yS_bhc1Vx_5jokQh3Oracc-ZIBsGE/edit?usp=sharing) (for existing team members)
* [Onboarding Checklist](https://docs.google.com/document/d/1DqbNBL6QIFBQlxqi7rWQiodFt6KvSp5fml3llu94Byw/edit?usp=sharing) (for new team members)
</details>

<details>
<summary>GitLab Ireland LTD</summary>

### Ireland

**Health and Safety**

* [Display Screen Equipment Regulations 2007](https://www.irishstatutebook.ie/eli/2007/si/299/made/en/print#partii-chapv)

* To ensure the physical and mental health and safety of our team members in Ireland, and to maintain compliance with local employment regulations, all team members in Ireland will complete a [home working checklist](https://forms.gle/bmXqNdH1xEw2bFTa8) at onboarding. This checklist will be reviewed annually. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, or you need to report an incident, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com).
</details>

<details>
<summary>GitLab Korea LTD</summary>

### Korea

</details>

<details>
<summary>GitLab LTD (UK)</summary>

### United Kingdom

</details>

<details>
<summary>GitLab PTY (Australia)</summary>

### Australia 

**Health and Safety**

To ensure the physical and mental health and safety of our team members in Australia, and to maintain compliance with local employment regulations, all team members in Australia will complete the following checklist at onboarding. This checklist will be reviewed annually. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, or you need to report an incident, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com).

* [Remote Work Checklist](https://docs.google.com/document/d/1_sHk3ksGLDVxBZsnO3pMD-U_R_Fy0Yyu/edit?usp=sharing&ouid=106298195226644329866&rtpof=true&sd=true)

</details>

<details>
<summary>GitLab PTY (New Zealand)</summary>

### New Zealand

**Health and Safety**

To ensure the physical and mental health and safety of our team members in New Zealand, and to maintain compliance with local employment regulations, all team members in Australia will complete the following survey at onboarding. The responses will be reviewed annually. If you think you may need accommodations in order to achieve and/or maintain a healthy and safe work environment, or you need to report an incident, please contact [Team Member Relations](mailto:teammemberrelations@gitlab.com).

* [Remote Work Checklist](https://forms.gle/DszZNkBA22Rhy3VW7)
</details>

<details>
<summary>GitLab Singapore PTE LTD</summary>

### Singapore

**Health and Safety**

* [How to Create an Ergonomic Workspace](https://about.gitlab.com/company/culture/all-remote/tips/#create-an-ergonomic-workspace)
* [Focus your Workspace](https://about.gitlab.com/company/culture/all-remote/getting-started/#focus-your-workspace)
* [Combating Burnout, Isolation, and Anxiety in a Remote Workplace](https://about.gitlab.com/company/culture/all-remote/mental-health/)
* [Considerations for a Productive Home Office](https://about.gitlab.com/company/culture/all-remote/workspace/#introduction)
* [Equipment Examples](https://about.gitlab.com/handbook/finance/expenses/#-not-sure-what-to-buy-equipment-examples)
* [Hardware Expense Guide](https://about.gitlab.com/handbook/finance/expenses/#-hardware)
 
**Data Protection/Privacy Policy**
* [Employee Privacy Policy](https://about.gitlab.com/handbook/legal/privacy/employee-privacy-policy/)
* [GitLab Privacy Policy](https://about.gitlab.com/privacy/)
 
**Workplace Harassment Policy**
* [Anti-Harassment Policy](https://about.gitlab.com/handbook/anti-harassment/#introduction)
* [Code of Business Conduct & Ethics](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/)
 
**Fair Employment Practices Policy**
* [GitLab PTE LTD Fair Employment Practices Policy](https://docs.google.com/document/d/1osJfO9BysOqjpt5iLnqygXX7B5Imb9NXIXA3KBtDCJk/edit?usp=sharing)
</details> 
