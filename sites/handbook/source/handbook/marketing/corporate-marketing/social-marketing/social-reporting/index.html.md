---
layout: handbook-page-toc
title: "Social Media Reporting"
description: Metric Definitions, Cadences, and Objectives
twitter_image: "/images/opengraph/handbook/social-marketing/social-handbook-reporting.png"
twitter_image_alt: "GitLab's Social Media Handbook branded image"
twitter_site: "gitlab"
twitter_creator: "gitlab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Social Media Reporting
Reporting on our brand's organic social media efforts is critical in determining if what we're doing is working and understanding our audiences better. This page outline specifics on our reporting, including definitions of metrics.

## Dashboards and Reports 
For GitLab Team Members only

| I'm looking for...                           | ...this report or dashboard will help.                                                    |
|----------------------------------------------|----------------------------------------------------------------------------------------|
| Overall Brand Social Performance             | [Sisense Brand Social Performance Dashboard ](https://app.periscopedata.com/app/gitlab/621921/Organic-Social-Media-Metrics-(for-GitLab-Brand-Channels))                                            |
| Sheet to enter raw performance data          | [Raw metrics sheetload for Sisense](https://docs.google.com/spreadsheets/d/1Lc3uLs7gpoYYu10cLlzqzxWCFISja1Df-FSvY_xBQU4/edit#gid=0) (available to social and data team only) |
| Ad equivalency dashboard for all social data | [Social Ad Equivalency Dashboard by objective and campaign](https://docs.google.com/spreadsheets/d/1sZwoUwnk5BXHrmRkAPkipgtMJZ8_stS-hMagTujym0k/edit?usp=sharing)                              |

## Where does our data come from? 
Our brand social team uses Sprout Social as our social media management software to schedule, engage, and report social media efforts. The majority of this data is already available natively across channels but is not curated or collated well for our purposes. **Our single source of truth for all organic brand social data is Sprout Social, however, for our wider team, Sisense acts as our data warehouse and visualizer.**

[Learn how we pull data from Sprout, add it to a sheetload, and get it into Sisense with this video](https://youtu.be/gol6eKcmIew?t=83) (available to team members logged into our GitLab Unfiltered YouTube account only).

## Social Media Metric Definitions

**Impressions** are the number of times that our content was displayed to users. There are a few caveats:
- Twitter: This includes when other users retweet our posts.
- LinkedIn: This does not include when other users share our posts.
- Facebook: the # of times that any content associated with our page, not just a post (think updating images), was displayed to a user.
- Instagram: This does not include profile visitors.
- We use impressions = views, as defined for Corporate Marketing. This is not our most important metric for social-only performance but is one of the few metrics directly attributable to our shared top-of-funnel activity across teams.

**Engagements** are the number of times that users took action on our posts during the reporting period. This includes likes, retweets, comments, saves, replies, reactions, and shares. Does not include quote retweets.

**Post Link Clicks** (Link clicks or clicks for short) is the number of times that users clicked on links from your posts during the reporting period

**Ad Value (or equivalency)** is the GitLab-specific social media advertising dollar equivalent organized by post objective
- impressions (measured by CPM or Cost Per Thousand impressions)
- link clicks (measured by CPC or Cost Per Click)
- Ad Equivalency is a value-driven metric that is entirely determined by our real-world social advertising spend. We'll evaluate the ad equivalency per channel/quarter average. 
- We present ad value as a dollar amount, determined by the sum of a reporting period's equivalent CPM + CPC costs

**Net Follower Growth** is the total number of new followers, less the amount of unfollows. 

**Follower Growth Rate** is a percentage of growth over a reporting period. While we may look at this month-over-month, it's best-reviewed quarter-over-quarter or year-over-year.

**Engagement Rate** is the number of times during a reporting period that users engaged with our posts as a percentage of impressions. This metric indicates how engaging particular content or campaigns are overall.

## Reporting Cadences 

Our cadences allow us regular reporting periods and the ability to be accountable to our team members in other groups or report performance regularly. The social team will not accommodate random reporting requests that were not previously required, agreed upon, or that the team feels inappropriate to use time or review the data.

### Create an FY Reporting Epic and link to all reports here throughout the year

These epics can act as a living warehouse in GitLab to pull data from quarterly progress or campaign specific wrap up issues. The social team ought to add a link to these reporting issues to the FY Performance epic. 

- [FY22 Performance Epic](https://gitlab.com/groups/gitlab-com/-/epics/1465)

### Monthly Reporting 

The social team will pull data for overall brand performance and select topics/campaigns before the end of the second week of the month following the reporting month (e.g., January data will be available before mid-February). [The Sisense dashboard](https://app.periscopedata.com/app/gitlab/621921/Organic-Social-Media-Metrics-(for-GitLab-Brand-Channels)) takes approximately 24-hours to refresh after raw data is added to our sheetload.

We will also add the necessary data to our [Ad Equivalency dashboard](https://docs.google.com/spreadsheets/d/1sZwoUwnk5BXHrmRkAPkipgtMJZ8_stS-hMagTujym0k/edit?usp=sharing) in Google Sheets. 

### Quarterly Reporting

We'll be able to review the full quarter of [overall brand performance in Sisense](https://app.periscopedata.com/app/gitlab/621921/Organic-Social-Media-Metrics-(for-GitLab-Brand-Channels)) following the refresh with the third month of data added, typically before the end of the second week of the new quarter.

When the social team adds the third month of data from a quarter to the [ad equivalency dashboard](https://docs.google.com/spreadsheets/d/1sZwoUwnk5BXHrmRkAPkipgtMJZ8_stS-hMagTujym0k/edit?usp=sharing), we'll include the average CPC and CPM by social channel, closing the metrics for the quarter. Note, ad value performance won't be available until after the quarter closes.

We'll wrap each quarter with an issue outlining performance and insights. Please use the [social-quarterly-report issue template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/social-quarterly-report.md). [Here's what a completed issue on quarterly performance looks like](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4647).

### Annual Reporting

We'll use a deck to outline our focuses and wins, mapped from the [FY21 review deck](https://docs.google.com/presentation/d/1nlnysWmeVShRK4-frKKkKfUdBPz96qBOAkt26RmZmAM/edit?usp=sharing).

Annual reports should encompass all consumption methods: Epics, issues, written summaries, a deck, and a recording of the deck with the social team reviewing. 

[Use the FY21 epic and issue outline to build the FY22 versions.](https://gitlab.com/groups/gitlab-com/marketing/corporate_marketing/-/epics/67)

Annual reporting should be communicated to campaign/topic stakeholders, the communications and corporate marketing team, and the marketing organization overall. Include as updates in the #marketing Slack channel.

## Reporting, Data, and User Behavior Limitations

With social media data, there will always be a set of limitations based on what metrics are available, how each channel defines them, how our tools collate the data, and whether or not other variables are in play.

- We can't currently combine Sprout tags to review overall performance (e.g.,) if we wanted to check out how a specific campaign performs, but to compare how owned links performed vs. no link posts, would require manual math). This is universal across our Sprout instance due to the mechanisms inside Sprout.
- Posts can only count towards one ad equivalency metric. A post is either for brand awareness and engagement on social channels or to generate clicks to a website. Because of this, we can qualify the ROI of a post according to the specific type of ad equivalency, but never more than one kind.
- Ad equivalency is not a 1:1 of what GitLab would have needed to pay as a part of social advertising. Posts written as ads are dramatically different than those crafted for organic social media. Specific targeting available in social advertising will also impact an ad's performance. So we must look at ad equivalency as a proxy for spending -- we're adding value, not eliminating ad budget.
- Ad equivalencies are not a metric we measure for growth. As GitLab's social media advertising becomes more efficient over time, the cost will decrease. As our social advertising costs decrease, each action's value for impressions or clicks that the ads are optimized for becomes cheaper to purchase. So, organic social media ad equivalency should reduce as our social advertising becomes more effective. We measure the ad equivalency as added value. 
- Many of the core metrics that the social team reports on have inverse relationships. Posts with higher impressions tend to have lower clicks (and the opposite is true as well). A post shared worldwide and has a high volume of engagements may have a low engagement rate, mainly if the impressions are high. There are other inverse relationships across other metrics. When crafting posts, the social team considers the one action we'd like users to take, and we try to organize the copy, assets, time, and channel to be in our favor. 

## Reporting Tips

- To review a specific metric per campaign/tag in Sprout, export all posts as a csv file, upload the csv file to Sheets, and then filter posts tagged with the tag we want to report by. This is not an equally weighted review across channels, however it does work to provide channel-specific reporting. 


