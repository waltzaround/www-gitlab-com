---
layout: markdown_page
title: "FY22-Q4 OKRs"
description: "View GitLabs Objective-Key Results for FY22 Q4. Learn more here!"
canonical_path: "/company/okrs/fy22-q4/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from November 1, 2021 to January 31, 2022.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2021-09-27 | CEO shares top goals with E-group for feedback |
| -4 | 2021-10-04 | CEO pushes top goals to this page |
| -4 | 2021-10-04 | E-group propose OKRs for their functions in [Epics and Issues nested under the CEO's OKRs](/company/okrs/#executives-propose-okrs-for-their-functions). These issues and epics are shared in #okrs Slack channel |
| -3 | 2021-10-18 | E-group 50 minute draft review meeting on 2021-10-18 |
| -2 | 2021-10-18 | E-group discusses with their respective teams and polish their OKRs |
| -1 | 2021-10-25 | CEO reports post links to final OKR Epics in #okrs slack channel and @ mention the CEO and CoS for approval |
| 0  | 2021-11-01 | CoS updates OKR page for current quarter to be active |


## OKRs

### 1. CEO: Encourage wider community engagement
[Epic 1609](https://gitlab.com/groups/gitlab-com/-/epics/1609)
   1. **CEO KR:** Develop a strategy to grow to 1000 contributors per month. [Issue 12480](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12480)
   1. **CEO KR:** Meet quarterly objectives for hyperscalers and partners. [Issue 12479](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12479)
   1. **CEO KR:** Achieve seven certifications with each more than 2,500 certificates issued. [Issue 12481](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12481)

### 2. CEO: Optimize GitLab Managed future
[Epic 1610](https://gitlab.com/groups/gitlab-com/-/epics/1610)
   1. **CEO KR:** Meet GitLab.com improvement goals (availability above 99.95%, free user RoI strategy, increase trial conversion from x% to y% on the path to z%). [Issue 12482](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12482)
   1. **CEO KR:** Meet SaaS improvement goals (increase customer empathy project in engineering, x customers live with Project Horse beta, launch storage visibility). [Issue 12483](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12483)
   1. **CEO KR:** Implement category creation plan. [Issue 12484](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12484)

### 3. CEO: Accelerate customer initiatives
[Epic 1611](https://gitlab.com/groups/gitlab-com/-/epics/1611)
   1. **CEO KR:** Improve SUS usability through completing 100% of quarterly initiatives related to Workspace, Learnability, and Foundations. [Issue 12485](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12485)
   1. **CEO KR:** First order large SAOs on yearly plan. [Issue 12486](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12486)
   1. **CEO KR:** Achieve X% of ARR on cloud licensing. [Issue 12487](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12487)


