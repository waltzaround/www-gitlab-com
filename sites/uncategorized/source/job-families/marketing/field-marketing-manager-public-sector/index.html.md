---
layout: job_family_page
title: "Public Sector Field Marketing Manager"
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/A67lWGfue_U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Public Sector Field Marketing Manager

The Public Sector Field Marketing Manager, US - Public Sector supports our US Public Sector team.

### Job Grade 

The Public Sector Field Marketing Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

The Public Sector Field Marketing Manager has the same expectations as other [Field Marketing Managers](/job-families/marketing/field-marketing-manager/), but specializes in the public sector in the United States.

### Requirements

The Public Sector Field Marketing Manager has the same requirements as other [Field Marketing Managers](/job-families/marketing/field-marketing-manager/), as well as additional public sector requirements.  

### Additional Requirements for US Public Sector

- Successful track record working with US Public Sector sales team, distributors, and channel partners.
- Knowledge of state & local, Civilian, DoD, and Intelligence Community agencies and ability to translate this into a cohesive marketing plan for the Public Sector.

## Career Ladder

The next step for individual contributors is to move to the Manager of role listed in the Field Marketing page](/job-families/marketing/#manager-field-marketing) or laterally to the [Account Based Marketing Manager](/job-families/marketing/account-based-marketing-manager/) job family.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a series of 45 minute interviews with the CMO, Director of Field Marketing, regional sales leader, and a seller who they would be paired with.
- Based on location, candidates may meet in person with any of the above.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email. The total compensation for this role listed in https://about.gitlab.com/job-families/marketing/field-marketing-manager/ is 100% base salary.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
