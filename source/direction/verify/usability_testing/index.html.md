---
layout: markdown_page
title: "Category Direction - Usability Testing"
description: "Usability testing is a technique in user-centered interaction design to evaluate a product by testing it on users and is an irreplaceable usability practice"
canonical_path: "/direction/verify/usability_testing/"
---

- TOC
{:toc}

## Usability Testing

GitLab's testing focus is mainly around the functional testing of code in software. We are interested in expanding our testing capabilities to include user-centered interaction evaluation. With the vision of Usability Testing, GitLab is embedding direct input on how real users use the system within your testing practice.

Usability testing is slightly different from user acceptance testing (UAT) in that the primary purpose of a usability test is to improve a design, rather than a gate to approve a deployment. At GitLab, we're believers in user-focused design without manual interference. Usability Testing's vision is meant to celebrate the designer and product manager as first-class parties in the development lifecycle. By adding features with them in mind, we can help them to contribute their best.

A related category geared toward the readability of your applications is [Accessibility Testing](https://about.gitlab.com/direction/verify/accessibility_testing/). As an end to end application, we offer other features involving the designer in the DevOps lifecycle, including [Design Management](https://about.gitlab.com/direction/create/design_management/) in the [Create](https://about.gitlab.com/direction/create/) stage. 

Interested in joining the conversation for this category? Please join us in the category's issues
where we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AUsability%20Testing)
- [Overall Vision](/direction/ops/#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

The next thing we are working on for Usability Testing is [&3085](https://gitlab.com/groups/gitlab-org/-/epics/3085) which will bring the ability to take and attach screenshots to comments from the Visual Review tool. This will help users simplify their workflow and not have to take a screenshot and attach it to the comment after it is created in the MR. This also allows users without an account or an account without permissions to add those screenshots which they cannot do today.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

* [Add screenshots to Visual Review Tool](https://gitlab.com/groups/gitlab-org/-/epics/3085)
* [Add full-screen annotations to Visual Review tool](https://gitlab.com/gitlab-org/gitlab/issues/10762)
* [Automatically wire in visual reviews in review apps](https://gitlab.com/gitlab-org/gitlab/-/issues/35322)

We may find in research that only some of these issues are needed to move the vision for this category forward.

## Competitive Landscape

Usability testing is a feature offered primarily through integrations by competitive products. Taking advantage of Review Apps, we're able to offer a much nicer experience with a manual review tool for Review Apps via [gitlab-org&960](https://gitlab.com/groups/gitlab-org/-/epics/960) where feedback can be requested on a review environment. This brings feedback much earlier into the development process and integrates that automatically with GitLab Merge Requests.

### Marker.io

[Marker.io](https://marker.io/) is a product that ships mostly as an extension to go into your browser and allows you to raise an issue with various issue providers (Trello, JIRA, GitHub and even GitLab).  It provides a very intuitive user interface for screenshots, annotations and other discussions.

### Netlify Deploy Previews

Netlify recently introduced new functionality to [Deploy Previews](https://www.netlify.com/products/deploy-previews/) that allows users to annotate, take screenshots and leave comments on a deploy preview. This alongside their integrations with various project management tools is another step towards allowing the non developer personas to push their feedback further left in the development cycle to speed up the feedback loop.

## Top Customer Success/Sales Issue(s)

The top issues for CS/Sales teams are centered around uptier features in [Premium](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AUsability%20Testing&label_name[]=GitLab%20Premium) and [Ultimate](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AUsability%20Testing&label_name[]=GitLab%20Ultimate). In this category, the top feature is building in a [comparison view of screenshots](https://gitlab.com/gitlab-org/gitlab/-/issues/36862). 

## Top Customer Issue(s)

The most popular issue for the category is [gitlab#27228](https://gitlab.com/gitlab-org/gitlab/-/issues/27228) which adds image annotations for the Visual Review tool.

An interesting use case we first heard about from a customer is designers wanting to see their previous comments for unresolved threads in the tool [(gitlab#325454)](https://gitlab.com/gitlab-org/gitlab/-/issues/325454). We think this is an interesting and powerful way product designers and product managers can engage with the Usability Category and more quickly give feedback to the developers they work with to deliver customer value.

## Top Internal Customer Issue(s)

For usability testing, our internal team has asked for built-in support for visual regression testing ([gitlab#26489](https://gitlab.com/gitlab-org/gitlab/issues/26489)).

## Top Vision Item(s)

When we think about the long term vision of the Usability category we see opportunities to build in support for [visual regression testing](https://gitlab.com/gitlab-org/gitlab/issues/26489) to help developers see visual degradations before they merge.
